Raytracing Freeimage
======================

Ce projet est un projet d'Imagerie Numérique de 4ème année à l'ESIEA Laval.

## Membres de l'équipe

- Pablo BONDIA-LUTTIAU
- Michaël GHESQUIERE
- Théophile HAMELIN
- Samuel LABLEE

## Installation

Ce projet à été réalisé avec CMake. Il a été testé sur Windows 10, Linux et macOS, en utilisant l'IDE [CLion](https://www.jetbrains.com/clion/)
(Visual Studio ne pouvant pas compiler du C++ sur macOS, il est réservé au développement .NET et Xamarin).

## Fonctionnalités

- Gestion des sphères 3D, des triangles, des tétraèdres, des rectangles et parallélépipèdes
- Modèle de matériau de Blinn-Phong
- Sauvegarde de l'image dans un fichier de sortie

## Bonus réalisés

- Gestion de multiples sources lumineuses, à ajouter à sa guise
- Gestion de la "matière" d'un objet, en lui donnant un coefficient de brillance
- Possibilité de donner une texture à un objet 3D (sphères uniquement)
- Gestion de l'antialiasing via un filtre vu dans le cours
- Gestion des réflexions sur les objets 3D
- Gestion de la réfraction (fonction faite mais pas implémentée dans le code)
- Génération de la documentation de notre code dans le dossier ``doc/``

## Screenshots

![Demo planets](screenshots/solar_system.bmp)
![Shiny sphere](screenshots/shiny_sphere.bmp)
![Two lightsources](screenshots/two_lightsources.bmp)
![Reflexion and antialiasing](screenshots/reflexion_antialiasing.bmp)

## License

The Aimeos TYPO3 extension is licensed under the terms of the GPL Open Source
license and is available for free.
