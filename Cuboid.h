#pragma once

#include "Vec3.h"
#include "Rectangle.h"


class Cuboid : public Object
{
private:
	/**
	 * The first face of the cuboid
	 */
	Rectangle m_face1;

	/**
	 * The second face of the cuboid
	 */
	Rectangle m_face2;

	/**
	 * The third face of the cuboid
	 */
	Rectangle m_face3;

	/**
	 * The 4th face of the cuboid
	 */
	Rectangle m_face4;

	/**
	 * The 5th face of the cuboid
	 */
	Rectangle m_face5;

	/**
	 * The 6th face of the cuboid
	 */
	Rectangle m_face6;

public:
	/**
	 * Constructor of the cuboid
	 * @param mColor The color of the cuboid
	 * @param pBrillianceAmount The brilliance of the cuboid
	 * @param mPoint1 the first vertex of the cuboid
	 * @param mPoint2 the second vertex of the cuboid
	 * @param mPoint3 the third vertex of the cuboid
	 * @param mPoint4 the 4th vertex of the cuboid
	 * @param mPoint5 the 5th vertex of the cuboid
	 * @param mPoint6 the 6th vertex of the cuboid
	 * @param mPoint7 the 7th vertex of the cuboid
	 * @param mPoint8 the 8th vertex of the cuboid
	 * @param p_reflect_coefficient the reflect coefficient
	 */
	Cuboid(const tagRGBQUAD &mColor, double pBrillianceAmount, const Vec3 &mPoint1,
		   const Vec3 &mPoint2, const Vec3 &mPoint3, const Vec3 &mPoint4, const Vec3 &mPoint5,
		   const Vec3 &mPoint6, const Vec3 &mPoint7, const Vec3 &mPoint8, double p_reflect_coefficient);

	/**
	 * Getter of the first face
	 * @return The first face
	 */
	[[nodiscard]] const Rectangle &getMFace1() const;

	/**
	 * Getter of the second face
	 * @return The second face
	 */
	[[nodiscard]] const Rectangle &getMFace2() const;

	/**
	 * Getter of the third face
	 * @return The third face
	 */
	[[nodiscard]] const Rectangle &getMFace3() const;

	/**
	 * Getter of the 4th face
	 * @return The 4th face
	 */
	[[nodiscard]] const Rectangle &getMFace4() const;

	/**
	 * Getter of the 5th face
	 * @return The 5th face
	 */
	[[nodiscard]] const Rectangle &getMFace5() const;

	/**
	 * Getter of the 6th face
	 * @return The 6th face
	 */
	[[nodiscard]] const Rectangle &getMFace6() const;

	/**
	 * Destructor of the cuboid
	 */
	~Cuboid() override;

	/**
	 * Compute the centroid of the cuboid formed by the 8 vertices in parameters
	 * @param mPoint1 The first vertex
	 * @param mPoint2 The second vertex
	 * @param mPoint3 The third vertex
	 * @param mPoint4 The 4th vertex
	 * @param mPoint5 The 5th vertex
	 * @param mPoint6 The 6th vertex
	 * @param mPoint7 The 7th vertex
	 * @param mPoint8 The 8th vertex
	 * @return The centroid position of the cuboid
	 */
	static Vec3 computeCentroid(const Vec3 &mPoint1, const Vec3 &mPoint2, const Vec3 &mPoint3, const Vec3 &mPoint4,
								const Vec3 &mPoint5, const Vec3 &mPoint6, const Vec3 &mPoint7, const Vec3 &mPoint8);

	/**
	 * Compute the normal at the vector in parameter for a cuboid
	 * @param p_point The point to compute the normal at
	 * @return The normal vector at the given vector
	 */
	Vec3 getNormalAt(Vec3 p_point) override;


};
