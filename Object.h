#pragma once

#include <FreeImage.h>
#include "Vec3.h"

/**
 * Class representing a 3D object
 */
class Object
{
protected:
	/**
	 * The position of the object
	 */
	Vec3 m_position;
	/**
	 * The color of the object
	 */
	RGBQUAD m_color;

	/**
	 * The brilliance amount. 0.0f is pure mat material, the more you put, the more brilliant the object will be
	 */
	double m_brilliance_amount;

	/**
	 * The reflect coefficient
	 */
	double m_reflect_coefficient;

	/**
	 * Texture of the object, if any
	 */
	FIBITMAP *m_texture;
public:
	/**
	 * Getter of the texture
	 * @return The object texture
	 */
	[[nodiscard]] FIBITMAP *getMTexture() const;

	/**
	 * Setter of the texture
	 * @param mTexture The texture to set
	 */
	void setMTexture(FIBITMAP *mTexture);

	/**
	 * Getter of the brilliance amount
	 * @return The brilliance amount of the object
	 */
	[[nodiscard]] double getMBrillianceAmount() const;

	/**
	 * Constructor for the object
	 * @param mPosition the position of the object
	 * @param mColor the color of the object
	 * @param p_brilliance_amount the amount of brilliance of the object
	 * @param p_reflect_coefficient the reflect coefficient
	 */
	Object(const Vec3 &mPosition, tagRGBQUAD mColor, double p_brilliance_amount, double p_reflect_coefficient);

	/**
	 * Getter of the object position
	 * @return The object position
	 */
	[[nodiscard]] const Vec3 &getMPosition() const;

	/**
	 * Getter of the object RGB color
	 * @return The object RGB color
	 */
	[[nodiscard]] const RGBQUAD &getMColor() const;

	/**
	 * Pure virtual method that will be defined in all the different objects (Sphere, Triangle...). It is used to compute a normal vector to a point.
	 * @param p_point The point to compute the normal at
	 * @return The normal vector to the point given in parameter
	 */
	virtual Vec3 getNormalAt(Vec3 p_point) = 0;

	/**
	 * Destructor
	 */
	virtual ~Object();

	[[nodiscard]] double getMReflectCoefficient() const;
};