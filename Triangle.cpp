#include "Triangle.h"
#include "MathVector.h"

Triangle::Triangle(tagRGBQUAD p_color, const Vec3 &p_point1, const Vec3 &p_point2, const Vec3 &p_point3,
				   double mBrillianceAmount, double p_reflect_coefficient) : Object(
		Triangle::computeCentroid(p_point1, p_point2, p_point3), p_color, mBrillianceAmount, p_reflect_coefficient)
{
	this->m_point1 = p_point1;
	this->m_point2 = p_point2;
	this->m_point3 = p_point3;

	/* Centroid of a triangle (Centroid = position):
	 * X = (x1 + x2 + x3)/3
	 * Y = (y1 + y2 + y3)/3
	 * Z = (z1 + z2 + z3)/3
	*/
}

Triangle::~Triangle() = default;

const Vec3 &Triangle::getMPoint1() const
{
	return m_point1;
}

const Vec3 &Triangle::getMPoint2() const
{
	return m_point2;
}

const Vec3 &Triangle::getMPoint3() const
{
	return m_point3;
}

Triangle::Triangle() : Object(Vec3(0, 0, 0), {0, 0, 0}, 0.f, 0.0f)
{}

Vec3 Triangle::computeCentroid(const Vec3 &p_p1, const Vec3 &p_p2, const Vec3 &p_p3)
{
	double x = (p_p1.getCoordinateX() + p_p2.getCoordinateX() + p_p3.getCoordinateX()) / 3.0;
	double y = (p_p1.getCoordinateY() + p_p2.getCoordinateY() + p_p3.getCoordinateY()) / 3.0;
	double z = (p_p1.getCoordinateZ() + p_p2.getCoordinateZ() + p_p3.getCoordinateZ()) / 3.0;

	return Vec3(x, y, z);
}

Vec3 Triangle::getNormalAt(Vec3 p_point)
{
	Vec3 v1 = this->m_point2 - this->m_point1;
	Vec3 v2 = this->m_point3 - this->m_point1;
	return MathVector::multiply(-1.0, MathVector::normalize(MathVector::vectorProduct(v1, v2)));
}
