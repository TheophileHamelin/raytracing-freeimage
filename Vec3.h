#pragma once

#include <iostream>

/**
* Class representing a 3 dimensions vector
*/
class Vec3
{
private:
	/**
	 * The X coordinate of the vector
	 */
	double m_x;
	/**
	 * The Y coordinate of the vector
	 */
	double m_y;
	/**
	 * The Z coordinate of the vector
	 */
	double m_z;

public:
	/**
	 * Constructor
	 * @param x the X coordinate
	 * @param y the Y coordinate
	 * @param z the Z coordinate
	 */
	Vec3(double x, double y, double z);

	/**
	 * Base constructor, initializes with 0
	 */
	Vec3();

	/**
	 * Getter for the X coordinate
	 * @return the X coordinate of the vector
	 */
	[[nodiscard]] double getCoordinateX() const;

	/**
	 * Getter for the Y coordinate
	 * @return the Y coordinate of the vector
	 */
	[[nodiscard]] double getCoordinateY() const;

	/**
	 * Getter for the Z coordinate
	 * @return the Z coordinate of the vector
	 */
	[[nodiscard]] double getCoordinateZ() const;

	/**
	 * Print the vector
	 */
	[[maybe_unused]] void toString() const;

	/**
	 * Destructor
	 */
	virtual ~Vec3();
};

/**
 * Compute the difference between two Vec3 (x1-x2, y1-y2, z1-z2)
 * @param v1 The first 3D vector
 * @param v2 The second 3D vector
 * @return The difference between two Vec3
 */
Vec3 operator-(const Vec3& v1, const Vec3& v2);

/**
 * Compute the addition between two Vec3 (x2+x1, y2+y1, z2+z1)
 * @param v1 The first 3D vector
 * @param v2 The second 3D vector
 * @return The addition between two Vec3
 */
Vec3 operator+(const Vec3& v1, const Vec3& v2);

/**
 * Compute the division between a Vec3 and a double (x/d, y/d, z/d)
 * @param v1 The Vec3
 * @param d The double
 * @return The Vec3 result
 */
Vec3 operator/(const Vec3& v1, double d);

