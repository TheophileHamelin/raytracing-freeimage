#include <cmath>
#include "LightSource.h"
#include "MathVector.h"
#include "IntersectionRayObjectManager.h"
#include "Sphere.h"
#include "Tetrahedron.h"

#define AMBIENT 0.1
#define DIFFUSE 0.9
#define SPECULAR 0.2
#define SHINY 100.0f


LightSource::LightSource(const Vec3 &mPosition) : m_position(mPosition)
{}

void LightSource::enlight(const Vec3 &p_cameraRay, Object *p_object, const Vec3 &p_nearestIntersection,
						  const std::vector<Object *> &p_objects, double &p_I_d, double &p_I_s, double &p_I_a)
{
	Vec3 L = this->m_position - p_nearestIntersection;
	double light_distance = MathVector::norm(L);
	L = MathVector::normalize(L);

	bool is_visible = true;
	Vec3 temp = Vec3(p_nearestIntersection);
	bool intersectionCheckBool;

	//Check if it is necessary to compute diffuse and specular light, ambient light will always be computed
	for (auto &obj : p_objects)
	{
		if (obj == p_object) continue;

		intersectionCheckBool = false;

		//Checking the type of the object to compute the intersection
		if (dynamic_cast<Sphere *>(p_object))
		{
			intersectionCheckBool = IntersectionRayObjectManager::intersectionSphere(p_object->getMPosition(),
																					 dynamic_cast<Sphere *>(p_object)->getMRadius(),
																					 p_nearestIntersection, L, temp);
		} else if (dynamic_cast<Triangle *>(p_object))
		{
			intersectionCheckBool = IntersectionRayObjectManager::intersectionTriangle(
					dynamic_cast<Triangle *>(p_object)->getMPoint1(), dynamic_cast<Triangle *>(p_object)->getMPoint2(),
					dynamic_cast<Triangle *>(p_object)->getMPoint3(), p_nearestIntersection, L, temp);
		} else if (dynamic_cast<Tetrahedron *>(p_object))
		{
			intersectionCheckBool = IntersectionRayObjectManager::intersectionTetrahedron(
					dynamic_cast<Tetrahedron *>(p_object)->getMBase(),
					dynamic_cast<Tetrahedron *>(p_object)->getMFace1(),
					dynamic_cast<Tetrahedron *>(p_object)->getMFace2(),
					dynamic_cast<Tetrahedron *>(p_object)->getMFace3(), p_nearestIntersection, L, temp);
		}

		if (intersectionCheckBool)
		{
			if (MathVector::norm(temp - p_nearestIntersection) < light_distance)
			{
				continue;
			}

			is_visible = false;
			break;
		}
	}

	double I_d = 0.0f, I_s = 0.0f;

	// If the object is DIRECTLY visible, compute diffuse and specular light
	if (is_visible)
	{
		Vec3 N = p_object->getNormalAt(p_nearestIntersection);
		double f_att = 1.0f / MathVector::norm(L);

		// f_att * I * (N . L)  => diffuse light intensity
		I_d = f_att * DIFFUSE * MathVector::scalarProduct(N, L);
		I_d = I_d > 0.0f ? I_d : 0.0f;

		// f_att * I * (N . H)^n => specular light intensity
		// H = median of L and V
		// We need to multiply p_cameraRay by -1 to inverse the direction of it
		I_s = f_att * SPECULAR *
			  std::pow(MathVector::scalarProduct(N, MathVector::median(L, MathVector::multiply(-1.0f, p_cameraRay))),
					   SHINY);
		I_s = I_s > 0.0f ? I_s : 0.0f;
	}


	p_I_s += I_s;
	p_I_d += I_d;
	p_I_a = AMBIENT;
}
