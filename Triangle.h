#pragma once

#include "Vec3.h"
#include "Object.h"

/**
 * Class representing a triangle, a 2D object
 */
class Triangle : public Object
{
private:
	/**
	 * First vertex of the triangle
	 */
	Vec3 m_point1;

	/**
	 * Second vertex of the triangle
	 */
	Vec3 m_point2;

	/**
	 * Third vertex of the triangle
	 */
	Vec3 m_point3;
public:
	/**
	 * Constructor of the triangle
	 * @param p_color The RGB color of the sphere
	 * @param p_point1 The first vertex of the triangle
	 * @param p_point2 The second vertex of the triangle
	 * @param p_point3 The third vertex of the triangle
	 * @param mBrillianceAmount The brilliance amount of the triangle
	 * @param p_reflect_coefficient the reflect coefficient
	 */
	Triangle(tagRGBQUAD p_color, const Vec3 &p_point1, const Vec3 &p_point2, const Vec3 &p_point3,
			 double mBrillianceAmount, double p_reflect_coefficient);

	/**
	 * Default constructor (unused)
	 */
	Triangle();

	/**
	 * Getter of the first vertex position
	 * @return The position of the first vertex
	 */
	[[nodiscard]] const Vec3 &getMPoint1() const;

	/**
	 * Getter of the second vertex position
	 * @return The position of the second vertex
	 */
	[[nodiscard]] const Vec3 &getMPoint2() const;

	/**
	 * Getter of the third vertex position
	 * @return The position of the third vertex
	 */
	[[nodiscard]] const Vec3 &getMPoint3() const;

	/**
	 * Compute the centroid of the triangle formed by the vertices in parameters
	 * @param p_p1 The first vertex
	 * @param p_p2 The second vertex
	 * @param p_p3 The third vertex
	 * @return The centroid position of the triangle formed by the three vertices
	 */
	static Vec3 computeCentroid(const Vec3 &p_p1, const Vec3 &p_p2, const Vec3 &p_p3);

	/**
	 * Destructor of triangle
	 */
	~Triangle() override;

	/**
	 * Compute the normal at the vector in parameter for a triangle
	 * @param p_point The point to compute the normal at
	 * @return The normal vector at the given vector
	 */
	Vec3 getNormalAt(Vec3 p_point) override;
};
