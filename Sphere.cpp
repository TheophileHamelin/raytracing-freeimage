#include "Sphere.h"
#include "MathVector.h"

Sphere::Sphere(const Vec3 &mPosition, tagRGBQUAD mColor, float mRadius, double mBrillianceAmount,
			   double p_reflect_coefficient)
		: Object(mPosition, mColor, mBrillianceAmount, p_reflect_coefficient),
		  m_radius(mRadius)
{}

float Sphere::getMRadius() const
{
	return m_radius;
}

Vec3 Sphere::getNormalAt(Vec3 p_point)
{
	return MathVector::normalize(p_point - this->getMPosition());
}
