var annotated_dup =
[
    [ "Camera", "class_camera.html", "class_camera" ],
    [ "IntersectionRayObjectManager", "class_intersection_ray_object_manager.html", null ],
    [ "LightSource", "class_light_source.html", "class_light_source" ],
    [ "MathVector", "class_math_vector.html", null ],
    [ "Object", "class_object.html", "class_object" ],
    [ "Scene", "class_scene.html", "class_scene" ],
    [ "Sphere", "class_sphere.html", "class_sphere" ],
    [ "Tetrahedron", "class_tetrahedron.html", "class_tetrahedron" ],
    [ "Triangle", "class_triangle.html", "class_triangle" ],
    [ "Vec3", "class_vec3.html", "class_vec3" ]
];