var searchData=
[
  ['raytracing_20freeimage_68',['Raytracing Freeimage',['../md__r_e_a_d_m_e.html',1,'']]],
  ['readme_2emd_69',['README.md',['../_r_e_a_d_m_e_8md.html',1,'']]],
  ['rectangle_70',['Rectangle',['../class_rectangle.html',1,'Rectangle'],['../class_rectangle.html#a7c798b7bfde5cda4b106b36d6672cf94',1,'Rectangle::Rectangle(tagRGBQUAD p_color, const Vec3 &amp;p_point1, const Vec3 &amp;p_point2, const Vec3 &amp;p_point3, const Vec3 &amp;p_point4, double mBrillianceAmount)'],['../class_rectangle.html#a8a933e0ebd9e80ce91e61ffe87fd577e',1,'Rectangle::Rectangle()']]],
  ['rectangle_2ecpp_71',['Rectangle.cpp',['../_rectangle_8cpp.html',1,'']]],
  ['rectangle_2eh_72',['Rectangle.h',['../_rectangle_8h.html',1,'']]],
  ['reflectedray_73',['reflectedRay',['../class_math_vector.html#a74437da224cf77ec4db79aeb5023f89a',1,'MathVector']]],
  ['refractedray_74',['refractedRay',['../class_math_vector.html#afb091d5297e4360f74730c407d322e1b',1,'MathVector']]],
  ['render_75',['render',['../class_scene.html#a4ddf2d16f371ee9533b3faf1dd5ddfb1',1,'Scene']]],
  ['renderobject_76',['renderObject',['../class_scene.html#a2c376c6062a624daf727cd6cc48275fd',1,'Scene']]]
];
