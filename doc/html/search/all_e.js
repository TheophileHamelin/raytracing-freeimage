var searchData=
[
  ['tetrahedron_90',['Tetrahedron',['../class_tetrahedron.html',1,'Tetrahedron'],['../class_tetrahedron.html#a05a9382e79a0bf573b6f9a46d73beaab',1,'Tetrahedron::Tetrahedron()']]],
  ['tetrahedron_2ecpp_91',['Tetrahedron.cpp',['../_tetrahedron_8cpp.html',1,'']]],
  ['tetrahedron_2eh_92',['Tetrahedron.h',['../_tetrahedron_8h.html',1,'']]],
  ['texture_5fheight_93',['TEXTURE_HEIGHT',['../_scene_8cpp.html#a75434db7403d999b9acc501f74dc6e74',1,'Scene.cpp']]],
  ['texture_5fwidth_94',['TEXTURE_WIDTH',['../_scene_8cpp.html#a1427df84856073155ef2c02b11dd7046',1,'Scene.cpp']]],
  ['tostring_95',['toString',['../class_vec3.html#af36dee7952df0c7bc5aee54d29bbb08d',1,'Vec3']]],
  ['triangle_96',['Triangle',['../class_triangle.html',1,'Triangle'],['../class_triangle.html#ad26cb06b9fc498cbd6b964bd0c3c01d9',1,'Triangle::Triangle(tagRGBQUAD p_color, const Vec3 &amp;p_point1, const Vec3 &amp;p_point2, const Vec3 &amp;p_point3, double mBrillianceAmount)'],['../class_triangle.html#aaefe4ed500c07918d30c6f0e286332c5',1,'Triangle::Triangle()']]],
  ['triangle_2ecpp_97',['Triangle.cpp',['../_triangle_8cpp.html',1,'']]],
  ['triangle_2eh_98',['Triangle.h',['../_triangle_8h.html',1,'']]]
];
