var searchData=
[
  ['tetrahedron_204',['Tetrahedron',['../class_tetrahedron.html#a05a9382e79a0bf573b6f9a46d73beaab',1,'Tetrahedron']]],
  ['tostring_205',['toString',['../class_vec3.html#af36dee7952df0c7bc5aee54d29bbb08d',1,'Vec3']]],
  ['triangle_206',['Triangle',['../class_triangle.html#ad26cb06b9fc498cbd6b964bd0c3c01d9',1,'Triangle::Triangle(tagRGBQUAD p_color, const Vec3 &amp;p_point1, const Vec3 &amp;p_point2, const Vec3 &amp;p_point3, double mBrillianceAmount)'],['../class_triangle.html#aaefe4ed500c07918d30c6f0e286332c5',1,'Triangle::Triangle()']]]
];
