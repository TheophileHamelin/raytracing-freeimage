var class_tetrahedron =
[
    [ "Tetrahedron", "class_tetrahedron.html#a05a9382e79a0bf573b6f9a46d73beaab", null ],
    [ "~Tetrahedron", "class_tetrahedron.html#a9f7c9a343292e6a97c718c5e4f3e6874", null ],
    [ "getMBase", "class_tetrahedron.html#ae6d1e7973509b4691398ecec62a4bf50", null ],
    [ "getMFace1", "class_tetrahedron.html#a814c8d7ceb98c734d1e291d901e78556", null ],
    [ "getMFace2", "class_tetrahedron.html#a938580651667802dc85eb15d06c159ea", null ],
    [ "getMFace3", "class_tetrahedron.html#a6f8fea2f1e03469fe323c4491178f363", null ],
    [ "getNormalAt", "class_tetrahedron.html#a8e2bb9e7850ade0f191e37f298975083", null ]
];