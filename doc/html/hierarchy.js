var hierarchy =
[
    [ "Camera", "class_camera.html", null ],
    [ "IntersectionRayObjectManager", "class_intersection_ray_object_manager.html", null ],
    [ "LightSource", "class_light_source.html", null ],
    [ "MathVector", "class_math_vector.html", null ],
    [ "Object", "class_object.html", [
      [ "Sphere", "class_sphere.html", null ],
      [ "Tetrahedron", "class_tetrahedron.html", null ],
      [ "Triangle", "class_triangle.html", null ]
    ] ],
    [ "Scene", "class_scene.html", null ],
    [ "Vec3", "class_vec3.html", null ]
];