var class_object =
[
    [ "Object", "class_object.html#a9828d02f7280f1122ed86c655c6d46f8", null ],
    [ "~Object", "class_object.html#ae8f5483f459e46687bd01e6f9977afd3", null ],
    [ "getMBrillianceAmount", "class_object.html#a1a617901c1f6a3e1e4a69ab4f72aa98a", null ],
    [ "getMColor", "class_object.html#a60dc91166d27562ab63ac5c8c6887569", null ],
    [ "getMPosition", "class_object.html#ad129d1111852d35fe149706acaf8e3c7", null ],
    [ "getMTexture", "class_object.html#a8ccfefde2ca72d2bb3a7e02e03ea00a6", null ],
    [ "getNormalAt", "class_object.html#afe25084ef0aea96b54b9e686fcdf4fd7", null ],
    [ "setMTexture", "class_object.html#a2b153db3d813370b7f8e493bd042e6b0", null ],
    [ "m_brilliance_amount", "class_object.html#a6acf738a323089f87ad11ab81c0c58d6", null ],
    [ "m_color", "class_object.html#ae6a5627e3b12d06aafabd37858de5808", null ],
    [ "m_position", "class_object.html#adfebf6b7b393d049a6ffa8ea683bfa6b", null ],
    [ "m_texture", "class_object.html#a5d89d83d7d0aa77ea55973eb8eb3bffa", null ]
];