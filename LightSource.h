#pragma once

#include <vector>
#include "Vec3.h"
#include "Object.h"

/**
 * Class representing a light source
 */
class LightSource
{
private:
	/**
	 * The position of the light source
	 */
	Vec3 m_position;

public:
	/**
	 * Constructor for a light source
	 * @param mPosition the position of the light source
	 */
	explicit LightSource(const Vec3 &mPosition);

	/**
	 * Method managing the light with objects
	 * @param p_cameraRay The ray traced from the camera to a screen pixel
	 * @param p_object The object to enlight
	 * @param p_nearestIntersection The nearest intersection between an object and a ray traced from the camera
	 * @param p_objects The objects of the scene
	 * @param p_I_d Diffuse light indices
	 * @param p_I_s Specular light indices
	 * @param p_I_a Ambient light indices
	 */
	void enlight(const Vec3 &p_cameraRay, Object *p_object, const Vec3 &p_nearestIntersection,
				 const std::vector<Object *> &p_objects, double &p_I_d, double &p_I_s, double &p_I_a);
};