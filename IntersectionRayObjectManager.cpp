//
// Created by theoh on 23/02/2021.
//

#include <cmath>
#include "IntersectionRayObjectManager.h"
#include "MathVector.h"

bool IntersectionRayObjectManager::intersectionSphere(const Vec3& p_sphereCenter, float p_sphereRadius, const Vec3& p_rayOrigin,
													  const Vec3& p_rayDirection, Vec3 &p_intersectionCoordinates)
{
	/* Method based on the resolution of this second degree equation :
		t^2.(dx^2+dx^2+dz^2)+2t.(x0.dx+y0.dy+z0.dz)+(x0^2+y0^2+z0^2)- R^2 = 0
		(dx,dy,dz) are the coordinates of the ray direction
		(xc,yc,zc) are the coordinates of the sphere center
		(x_0,y_0,z_0) are the coordinates of the ray origin
		x0 = x_0 - xc
		y0 = y_0 - yc
		z0 = z_0 - zc
		R is the sphere radius
	*/

	//Check if the point belongs to the sphere
	double x0 = p_rayOrigin.getCoordinateX() - p_sphereCenter.getCoordinateX();
	double y0 = p_rayOrigin.getCoordinateY() - p_sphereCenter.getCoordinateY();
	double z0 = p_rayOrigin.getCoordinateZ() - p_sphereCenter.getCoordinateZ();

	double a = p_rayDirection.getCoordinateX() * p_rayDirection.getCoordinateX() +
			   p_rayDirection.getCoordinateY() * p_rayDirection.getCoordinateY() +
			   p_rayDirection.getCoordinateZ() * p_rayDirection.getCoordinateZ();
	double b = 2 * (x0 * p_rayDirection.getCoordinateX() + y0 * p_rayDirection.getCoordinateY() +
					z0 * p_rayDirection.getCoordinateZ());
	double c = (x0 * x0 + y0 * y0 + z0 * z0) - (p_sphereRadius * p_sphereRadius);
	double delta = (b * b) - (4 * a * c);

	if (delta < 0)
	{
		//Delta < 0 : No intersection
		return false;
	} else if (delta > 0)
	{
		//Delta > 0 : 2 intersections, we need to find the nearest point
		double t1 = (-b - sqrt(delta)) / 2 * a;
		double t2 = (-b + sqrt(delta)) / 2 * a;
		if (t1 > 0 || t2 > 0)
		{
			if (t1 < t2)
			{
				p_intersectionCoordinates = p_rayOrigin + MathVector::multiply(t1, p_rayDirection);
			} else
			{
				p_intersectionCoordinates = p_rayOrigin + MathVector::multiply(t2, p_rayDirection);
			}
			return true;
		} else
		{
			return false;
		}
	} else
	{
		//Delta = 0 : The point is tangent to the sphere
		double t0 = -b / (2 * a);

		//Computing the coordinates of the intersection
		p_intersectionCoordinates = p_rayOrigin + MathVector::multiply(t0, p_rayDirection);
		return true;
	}
}

bool
IntersectionRayObjectManager::intersectionTriangle(const Vec3& p_trianglePoint1, const Vec3& p_trianglePoint2, const Vec3& p_trianglePoint3,
												   const Vec3& p_rayOrigin, const Vec3& p_rayDirection,
												   Vec3 &p_intersectionCoordinates)
{
	// Check if the plane formed by the triangle and the ray are parallel
	Vec3 normalPlane = MathVector::normalPlane(p_trianglePoint1, p_trianglePoint2, p_trianglePoint3);

	if (MathVector::scalarProduct(normalPlane, p_rayDirection) == 0)
	{
		//They are parallel, no intersection
		return false;
	}

	// Compute d parameter using equation 2
	double d = MathVector::scalarProduct(normalPlane, p_trianglePoint1);

	// Compute t (equation 3)
	double t = (MathVector::scalarProduct(normalPlane, p_rayOrigin) + d) /
			   MathVector::scalarProduct(normalPlane, p_rayDirection);

	if (t < 0.0)
	{
		//The triangle is behind the ray
		return false;
	}

	//Compute intersection
	p_intersectionCoordinates = p_rayOrigin + MathVector::multiply(t, p_rayDirection);

	//Check if the intersection is inside or outside the triangle
	Vec3 v;

	//Edge 1
	Vec3 edge1 = p_trianglePoint2 - p_trianglePoint1;
	Vec3 intersectionP1 = p_intersectionCoordinates - p_trianglePoint1;
	v = MathVector::vectorProduct(edge1, intersectionP1);
	if (MathVector::scalarProduct(normalPlane, v) < 0)
	{
		return false;
	}

	//Edge 2
	Vec3 edge2 = p_trianglePoint3 - p_trianglePoint2;
	Vec3 intersectionP2 = p_intersectionCoordinates - p_trianglePoint2;
	v = MathVector::vectorProduct(edge2, intersectionP2);
	if (MathVector::scalarProduct(normalPlane, v) < 0)
	{
		return false;
	}

	//Edge 3
	Vec3 edge3 = p_trianglePoint1 - p_trianglePoint3;
	Vec3 intersectionP3 = p_intersectionCoordinates - p_trianglePoint3;
	v = MathVector::vectorProduct(edge3, intersectionP3);
	if (MathVector::scalarProduct(normalPlane, v) < 0)
	{
		return false;
	}

	//The intersection is inside
	return true;
}

bool IntersectionRayObjectManager::intersectionTetrahedron(const Triangle& p_base, const Triangle& p_face1, const Triangle& p_face2,
														   const Triangle& p_face3, const Vec3& p_rayOrigin, const Vec3& p_rayDirection,
														   Vec3 &p_intersectionCoordinates)
{
	if (IntersectionRayObjectManager::intersectionTriangle(p_base.getMPoint1(), p_base.getMPoint2(),
														   p_base.getMPoint3(), p_rayOrigin, p_rayDirection,
														   p_intersectionCoordinates))
	{
		return true;
	}

	if (IntersectionRayObjectManager::intersectionTriangle(p_face1.getMPoint1(), p_face1.getMPoint2(),
														   p_face1.getMPoint3(), p_rayOrigin, p_rayDirection,
														   p_intersectionCoordinates))
	{
		return true;
	}

	if (IntersectionRayObjectManager::intersectionTriangle(p_face2.getMPoint1(), p_face2.getMPoint2(),
														   p_face2.getMPoint3(), p_rayOrigin, p_rayDirection,
														   p_intersectionCoordinates))
	{
		return true;
	}

	if (IntersectionRayObjectManager::intersectionTriangle(p_face3.getMPoint1(), p_face3.getMPoint2(),
														   p_face3.getMPoint3(), p_rayOrigin, p_rayDirection,
														   p_intersectionCoordinates))
	{
		return true;
	}

	return false;
}


bool IntersectionRayObjectManager::intersectionRectangle(Vec3 p_rectanglePoint1, Vec3 p_rectanglePoint2,
                                                         Vec3 p_rectanglePoint3, Vec3 p_rectanglePoint4,
                                                         Vec3 p_rayOrigin, Vec3 p_rayDirection,
                                                         Vec3 &p_intersectionCoordinates) {
    // Check if the plane formed by the rectangle and the ray are parallel
    Vec3 normalPlane = MathVector::normalPlane(p_rectanglePoint1, p_rectanglePoint2, p_rectanglePoint3); //only 3 points to define a plane

    if(MathVector::scalarProduct(normalPlane, p_rayDirection) == 0){
        //They are parallel, no intersection
        return false;
    }

    // Compute d parameter using equation 2
    double d = MathVector::scalarProduct(normalPlane, p_rayDirection);

    // Compute t (equation 3)
    double t = MathVector::scalarProduct(normalPlane, Rectangle::computeCentroid(p_rectanglePoint1,p_rectanglePoint2,p_rectanglePoint3,p_rectanglePoint4)-p_rayOrigin) / d;

    if(t < 0.0){
        //The rectangle is behind the ray
        return false;
    }

    //Compute intersection
    p_intersectionCoordinates = p_rayOrigin + MathVector::multiply(t, p_rayDirection);
    //Check if the intersection is inside or outside the rectangle
    Vec3 v;

    //Edge 1
    Vec3 edge1 = p_rectanglePoint2-p_rectanglePoint1;
    Vec3 intersectionP1 = p_intersectionCoordinates - p_rectanglePoint1;
    v = MathVector::vectorProduct(edge1, intersectionP1);
    if(MathVector::scalarProduct(normalPlane, v) < 0){
        return false;
    }

    //Edge 2
    Vec3 edge2 = p_rectanglePoint3-p_rectanglePoint2;
    Vec3 intersectionP2 = p_intersectionCoordinates - p_rectanglePoint2;
    v = MathVector::vectorProduct(edge2, intersectionP2);
    if(MathVector::scalarProduct(normalPlane, v) < 0){
        return false;
    }

    //Edge 3
    Vec3 edge3 = p_rectanglePoint4-p_rectanglePoint3;
    Vec3 intersectionP3 = p_intersectionCoordinates - p_rectanglePoint3;
    v = MathVector::vectorProduct(edge3, intersectionP3);
    if(MathVector::scalarProduct(normalPlane, v) < 0){
        return false;
    }

    //Edge 4
    Vec3 edge4 = p_rectanglePoint1-p_rectanglePoint4;
    Vec3 intersectionP4 = p_intersectionCoordinates - p_rectanglePoint4;
    v = MathVector::vectorProduct(edge4, intersectionP4);
    if(MathVector::scalarProduct(normalPlane, v) < 0){
        return false;
    }

    //The intersection is inside
    return true;
}


bool IntersectionRayObjectManager::intersectionCuboid(Rectangle p_face1, Rectangle p_face2, Rectangle p_face3,
                                                      Rectangle p_face4, Rectangle p_face5, Rectangle p_face6,
                                                      Vec3 p_rayOrigin, Vec3 p_rayDirection,
                                                      Vec3 &p_intersectionCoordinates) {
    if(IntersectionRayObjectManager::intersectionRectangle(p_face1.getMPoint1(), p_face1.getMPoint2(), p_face1.getMPoint3(), p_face1.getMPoint4(), p_rayOrigin, p_rayDirection, p_intersectionCoordinates)){
        return true;
    }

    if(IntersectionRayObjectManager::intersectionRectangle(p_face2.getMPoint1(), p_face2.getMPoint2(), p_face2.getMPoint3(), p_face2.getMPoint4(), p_rayOrigin, p_rayDirection, p_intersectionCoordinates)){
        return true;
    }

    if(IntersectionRayObjectManager::intersectionRectangle(p_face3.getMPoint1(), p_face3.getMPoint2(), p_face3.getMPoint3(), p_face3.getMPoint4(), p_rayOrigin, p_rayDirection, p_intersectionCoordinates)){
        return true;
    }

    if(IntersectionRayObjectManager::intersectionRectangle(p_face4.getMPoint1(), p_face4.getMPoint2(), p_face4.getMPoint3(), p_face4.getMPoint4(), p_rayOrigin, p_rayDirection, p_intersectionCoordinates)){
        return true;
    }

    if(IntersectionRayObjectManager::intersectionRectangle(p_face5.getMPoint1(), p_face5.getMPoint2(), p_face5.getMPoint3(), p_face5.getMPoint4(), p_rayOrigin, p_rayDirection, p_intersectionCoordinates)){
        return true;
    }

    if(IntersectionRayObjectManager::intersectionRectangle(p_face6.getMPoint1(), p_face6.getMPoint2(), p_face6.getMPoint3(), p_face6.getMPoint4(), p_rayOrigin, p_rayDirection, p_intersectionCoordinates)){
        return true;
    }

    return false;
}
