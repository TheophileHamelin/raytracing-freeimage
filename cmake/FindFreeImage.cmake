FIND_PATH(FreeImage_INCLUDE_DIR FreeImage.h HINTS ../FreeImage)

FIND_LIBRARY(FreeImage_LIBRARY NAMES FreeImage HINTS ../FreeImage)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(FreeImage DEFAULT_MSG FreeImage_INCLUDE_DIR FreeImage_LIBRARY)