#pragma once

#include "Vec3.h"
#include "Triangle.h"

/**
* Class containing useful methods to compute vectors
*/
class MathVector
{
public:
    /**
     * Normalize a 3D vector
     * @param v The vector to normalize
     * @return The normalized vector
     */
	static Vec3 normalize(const Vec3 &v);

	/**
	 * Compute the norm of a 3D vector
	 * @param v The vector
	 * @return The norm of the vector
	 */
	static double norm(const Vec3& v);

	/**
	 * Compute the dot product between two Vec3
	 * @param v1 The first Vec3
	 * @param v2 The second Vec3
	 * @return The dot product
	 */
	static double scalarProduct(const Vec3& v1, const Vec3& v2);

	/**
	 * Compute the cross product between two Vec3
	 * @param v1 The first Vec3
	 * @param v2 The second Vec3
	 * @return The cross product
	 */
	static Vec3 vectorProduct(const Vec3& v1, const Vec3& v2);

	/**
	 * Compute the multiplication between a double and a Vec3
	 * @param i The double
	 * @param v The Vec3
	 * @return The Vec3 result of the multiplcation
	 */
	static Vec3 multiply(double i, const Vec3& v);

	/**
	 * Compute a reflected ray
	 * @param normal The normal to the intersection point
	 * @param incident The incident Vec3
	 * @return The reflected ray Vec3
	 */
	static Vec3 reflectedRay(const Vec3 &normal, const Vec3 &incident);

    /**
     * Compute a refracted ray
     * @param normal The normal to the intersection point
     * @param incident The incident Vec3
     * @return The refracted ray Vec3
     */
	[[maybe_unused]] static Vec3 refractedRay(const Vec3& normal, const Vec3& incident, double refractionIndex);

    /**
     * Compute the normal to a plane
     * @param v1 The first point belonging to the plane
     * @param v2 The second point belonging to the plane
     * @param v3 The third point belonging to the plane
     * @return The normal to the plane defined by the three Vec3
     */
	static Vec3 normalPlane(const Vec3& v1, const Vec3& v2, const Vec3& v3);

	/**
	 * Compute the median vector between two vectors
	 * @param v1 the first vector
	 * @param v2 the second vector
	 * @return a median vector between v1 and v2, given by (v1 + v2) / norm(v1 + v2)
	 */
	static Vec3 median(const Vec3 &v1, const Vec3 &v2);

	/**
	 * Compute the UV coordinates for a given point in space.
	 * This is useful for texture mapping between a point in an image and its 3D coordinate.
	 * NOTE : THIS IS ONLY APPLICABLE FOR SPHERES RIGHT NOW
	 * @param v1 the point to transform into UV coordinates
	 * @param u the U coordinate
	 * @param v the V coordinate
	 */
	static void getUVForVector(const Vec3 &v1, double &u, double &v);
};

